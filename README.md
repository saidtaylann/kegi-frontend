# Kegi | Egitim Platformu

### **_Geliştirme aşamasındadır._**

<p style="margin-top:10px;">**Kegi; Canlı derslere girilip ödevlerin verilebildiği, puanlamaların yapılabildiği kapsamlı bir eğitim platformu
Bunun yanı sıra sıradan kullanıcılar için de canlı toplantı seçenekleri de mevcuttur.**</p>

## Neler Yapıldı?
- [x] Canlı toplantılar oluşturma (sadece kayıtlı kullanıcılar)
- [x] Ekran paylaşımı
- [x] Toplantı içi yetkilendirme (Oda sahibi, yetkili, katılımcı...)
- [x] Bütün (yetkisiz) kullanıcıların kamera ve mikrofon yetkilerini alma/verme
- [x] Toplantı içi genel sohbet ekranı
- [x] Katılımcılar listesi
- [x] İstenilen kullanıcının kamera, mikrofon ve ekran paylaşımı yetkisini alma/verme
- [x] Kullanıcıların rollerini değiştirme
- [x] Uygulama kullanıcılarının profillerini görüntüleme ve güncellemesi

## İstemcide Hangi Araçlar Kullanıldı?
1.  VueJS (Javscript)
2. Tailwind
3. [Flowbite](https://flowbite.com/)
4. Socket.io-client
5. [Ion SFU](https://github.com/ionorg/ion-sfu)

Projenin sunucu reposuna [gitmek için tıklayınız.](https://gitlab.com/saidtaylann/kegi-backend)


>  **Uygulama birden çok bileşen ve servisten oluştuğu için uygulamayı indirip başlatmak zor olacaktır. Bu yüzden nasıl kurulduğu anlatılmamıştır**

## Projeden Kareler

1. Toplantı Ekranı </br>
   <img src="./images/main.png" width=50%>

2. Ekran Paylaşma </br>
   <img src="./images/sharingScreen.png" width=50%>

3. Katılımcılar </br>
   <img src="./images/contextMenu.png" width=50%>

4. Sohbet </br>
   <img src="./images/chat.png" width=50%>

5. Mikrofon ve Kamera Kapatma </br>
   <img src="./images/closeCam&Mic.png" width=50%>

6. Toplantıdan Ayrılma </br>
   <img src="./images/leave.png" width=50%>
