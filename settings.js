const settings = {
  FILE_STORAGE_URL: "https://d1x73mf8qcw8er.cloudfront.net",
  DOMAIN: "https://kegi.netlify.app",
  TOP_LEVEL_DOMAIN: "app",
};

export default settings;
