import axios from "axios";

const serverUrl = "http://localhost:3000";
const customAxios = axios.create();

customAxios.interceptors.request.use(
  (req) => {
    req.headers.withCredentials = "include";
    req.headers.Authorization = localStorage.getItem("A_T")
      ? `Bearer ${localStorage.getItem("A_T")}`
      : "Bearer ";
    return req;
  },
  (err) => {
    return Promise.reject(err);
  }
);

customAxios.interceptors.response.use(
  (response) => {
    if (response.body === "TokenExpired") {
      axios
        .post(
          `${serverUrl}/auth/refresh`,
          {},
          { LID: localStorage.getItem("LID") }
        )
        .then((response) => response);
    }
    return response;
  },
  (error) => {
    if (error.response && error.response.data) {
      return Promise.reject(error.response.data);
    }
    return Promise.reject(error.message);
  }
);

export default customAxios;
