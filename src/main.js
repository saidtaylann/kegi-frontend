import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import { RingLoader as loader } from "vue3-spinner";
//style.css is for tailwind için
import "./style.css";
import Vue3Toastify from "vue3-toastify";
import "@imengyu/vue3-context-menu/lib/vue3-context-menu.css";
import ContextMenu from "@imengyu/vue3-context-menu";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";

const app = createApp(App);
app.use(createPinia());
app.component("Loader", loader);
app.use(ContextMenu);

const vuetify = createVuetify({
  components,
  directives,
});

app.use(Vue3Toastify, {
  autoClose: 3000,
});

app.use(vuetify);

app.use(router);

app.mount("#app");
