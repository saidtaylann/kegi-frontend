import {
  initCarousels,
  initCollapses,
  initDismisses,
  initDrawers,
  initDropdowns,
  initModals,
  initPopovers,
  initTabs,
  initTooltips,
} from "flowbite";

export const startFlowBiteJS = () => {
  // initialize components based on data attribute selectors
  initCollapses();
  initDismisses();
  initDrawers();
  initDropdowns();
  initModals();
  initPopovers();
  initTabs();
  initTooltips();
};
