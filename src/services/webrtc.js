import { Client, LocalStream } from "ion-sdk-js";
import { IonSFUJSONRPCSignal } from "ion-sdk-js/lib/signal/json-rpc-impl";
import { watch, ref } from "vue";

class WebRTC {
  #videoClient;
  #sharingScreenClient;
  #videoSignal;
  #screenSignal;
  #sharedScreens = ref([]);
  #participantVideos = ref([]);
  #dataChannel;
  #selfVideo;
  #selfScreen;

  constructor() {}

  async makeConfig(roomStore, userStore) {
    const webrtcConfig = {
      iceServers: [
        {
          urls: "stun:stun.l.google.com:19302",
        },
      ],
    };
    this.#videoSignal = new IonSFUJSONRPCSignal("ws://localhost:7001/ws");
    this.#screenSignal = new IonSFUJSONRPCSignal("ws://localhost:7002/ws");
    this.#videoClient = new Client(this.#videoSignal, webrtcConfig);
    this.#sharingScreenClient = new Client(this.#screenSignal, webrtcConfig);

    const roomSessionId = roomStore.getActiveSession.room_session_id;
    this.#videoSignal.onopen = async () => {
      this.#screenSignal.onopen = async () => {
        this.#sharingScreenClient.join(
          `${roomSessionId}`,
          userStore.getWebsocketID
        );
        this.#videoClient.join(`${roomSessionId}`, userStore.getWebsocketID);
        this.listenClients();
        await this.stream(roomStore, userStore.getWebsocketID);
        this.startSessionMessaging(roomStore);
      };
    };
  }

  listenClients() {
    this.getSpeaker();
    this.#videoClient.ontrack = (track, stream) => {
      if (track.kind === "video") {
        this.#participantVideos.value.push({
          srcObject: stream,
          streamId: stream.id,
          self: false,
          isSpeaking: false,
        });
      }
      stream.onremovetrack = () => {
        this.#participantVideos.value = this.#participantVideos.value.filter(
          (v) => v.streamId !== stream.id
        );
      };
    };

    this.#sharingScreenClient.ontrack = (track, stream) => {
      if (track.kind === "video") {
        this.#sharedScreens.value.push({
          srcObject: stream,
          streamId: stream.id,
          self: false,
        });
      }
      this.#sharingScreenClient.on = () => {
        console.log("onmute tetikendi");
        this.#sharedScreens.value = this.#sharedScreens.value.filter(
          (v) => v.streamId !== stream.id
        );
      };
    };
  }

  async stream(roomStore, websocketID) {
    const activeSessionId = roomStore.getActiveSession.room_session_id;
    const userPreferences = roomStore.getActiveSession.userPreferences;

    watch(
      () => userPreferences.mic.isOpen,
      (newV, oldV) => {
        this.#selfVideo.getAudioTracks()[0].enabled = newV;
      }
    );

    watch(
      () => userPreferences.cam.isOpen,
      (newV, oldV) => {
        this.#selfVideo.getVideoTracks()[0].enabled = newV;
      }
    );
    LocalStream.getUserMedia({
      resolution: "hd",
      audio: true,
      video: true,
    })
      .then((stream) => {
        this.#selfVideo = stream;
        roomStore.joinSession(activeSessionId, websocketID, {
          isCamOpen: true,
          isMicOpen: true,
          streamId: stream.id,
          isCamAuthorized: true,
          isMicAuthorized: true,
          isSharingScreenAuthorized: true,
        });
        this.#participantVideos.value.push({
          srcObject: stream,
          streamId: stream.id,
          self: true,
          isSpeaking: false,
        });
        this.#videoClient.publish(stream);
      })
      .catch(console.error);
  }

  getparticipantVideos() {
    return this.#participantVideos;
  }

  startSessionMessaging(roomStore) {
    this.#dataChannel = this.#videoClient.createDataChannel(
      `${roomStore.getActiveSession.room_session_id}`
    );
    this.#dataChannel.onmessage = (e) => {
      const jsonData = JSON.parse(e.data);
      if (jsonData.to === "all") {
        roomStore.addSessionMessage(jsonData);
      } else if (typeof jsonData.to === "string") {
        // eğer gönderilen kişiler 'all' hariç bir string ise bu mesaj, bir gruba gönderilmiş demektir.
        // Örneğin kişi, sweets adlı bir gruba dahil ise mesajlara eklenir.
      }
    };

    this.#videoClient.ondatachannel = (e) => {
      const dataChannel = e.channel;
      dataChannel.onmessage = (e) => {
        const jsonData = JSON.parse(e.data);
        if (jsonData.to === "all") {
          roomStore.addSessionMessage(jsonData);
        } else if (typeof jsonData.to === "string") {
          // eğer gönderilen kişiler 'all' hariç bir string ise bu mesaj, bir gruba gönderilmiş demektir.
          // Örneğin kişi, sweets adlı bir gruba dahil ise mesajlara eklenir.
        }
      };
    };
  }

  sendMessage(body, addMessageFunc) {
    this.#dataChannel.send(JSON.stringify(body));
    addMessageFunc(body);
  }

  shareScreen(roomStore, websocketId) {
    const userPreferences = roomStore.getActiveSession.userPreferences;
    watch(
      () => userPreferences.sharingScreen.isActive,
      (newV, oldV) => {
        this.#selfScreen.getVideoTracks()[0].enabled = newV;
      }
    );

    LocalStream.getDisplayMedia({
      audio: true,
      video: true,
    }).then((stream) => {
      this.#selfScreen = stream;
      this.#sharedScreens.value.push({
        srcObject: stream,
        streamId: stream.id,
        self: true,
        isSpeaking: false,
      });
      console.log("stream", stream);
      const videoTrack = stream.getVideoTracks()[0];
      videoTrack.onended = () => {
        this.stopSharing(roomStore, websocketId);
        this.#sharedScreens.value = this.#sharedScreens.value.filter(
          (v) => v.streamId !== stream.id
        );
        roomStore.setActiveSessionPreference(
          "sharingScreen",
          "isActive",
          false
        );
      };
      roomStore.setActiveSessionPreference("sharingScreen", "isActive", true);
      roomStore.shareScreen(
        roomStore.getActiveSession.room_session_id,
        websocketId,
        stream.id
      );
      this.#sharingScreenClient.publish(stream);
    });
  }

  stopSharing(roomStore, websocketId) {
    const streamIndex = this.#sharedScreens.value.findIndex(
      (s) => s.self === true
    );
    console.log("roomStore", roomStore);
    this.#selfScreen.getVideoTracks()[0].stop();
    roomStore.stopSharingScreen(
      roomStore.getActiveSession.room_session_id,
      websocketId
    );
    this.removeSharingScreen(this.#selfScreen.id);
    this.#selfScreen.unpublish();
    this.#sharedScreens.value.splice(streamIndex, 1);
    roomStore.setActiveSessionPreference("sharingScreen", "isActive", false);
    console.log("stop sharing");
  }

  getSharedScreens() {
    return this.#sharedScreens;
  }

  removeParticipant(streamId) {
    this.#participantVideos.value = this.#participantVideos.value.filter(
      (p) => p.streamId !== streamId
    );
  }

  removeSharingScreen(streamId) {
    const index = this.#sharedScreens.value.findIndex(
      (s) => s.streamId === streamId
    );
    this.#sharedScreens.value.splice(index, 1);
  }

  leave(roomStore, websocketId, roomStoreLeaveFunc) {
    console.log("leave tetiklendi");
    this.#sharedScreens.value = [];
    this.#sharingScreenClient.leave();

    this.#participantVideos.value = [];

    this.#selfVideo.getVideoTracks().forEach((t) => t.stop());
    this.#selfVideo.getAudioTracks().forEach((t) => t.stop());
    this.#videoClient.leave();

    this.#videoSignal.close();
    this.#screenSignal.close();
    roomStoreLeaveFunc();
  }

  getSpeaker() {
    this.#videoClient.onspeaker = (speakerIds) => {
      this.#participantVideos.value.forEach((p) => {
        if (speakerIds.includes(p.streamId)) {
          p.isSpeaking = true;
        } else {
          p.isSpeaking = false;
        }
      });
    };
  }
}

export const WebRTCService = new WebRTC();
