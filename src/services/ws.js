import io from "socket.io-client";
import { ref } from "vue";

class WebsocketService {
  #socket;
  #isConnected = ref(false);
  #wsUrl = "ws://localhost:3000";
  constructor() {
    this.#socket = io.connect(this.#wsUrl, {
      withCredentials: true,
    });
    this.#socket.on("connect", () => {
      console.log("bağlandı");
      console.log("this.#socket", this.#socket);
      this.#isConnected.value = true;
    });
  }

  emit(eventName, data) {
    const accessToken = localStorage.getItem("A_T");
    this.#socket.emit(eventName, { ...data, accessToken });
  }

  isConnected() {
    return this.#isConnected.value;
  }

  disconnect() {
    if (this.#socket) {
      this.#socket.disconnect();
    }
  }

  getSocket() {
    return this.#socket;
  }

  getSocketID() {
    return this.#socket.id;
  }

  startUserSession(LID) {
    console.log("user session");
    this.emit("startUserSession", { LID });
  }

  createRoomSession(roomCode, sessionPreferences) {
    this.emit("createRoomSession", {
      roomCode,
      sessionPreferences,
    });
  }

  // odaları açıp kapatabilecek kişiler sadece yetkililer olmasına rağmen userSessionId ile cache'deki is_open bilgisi
  // güncelleneceği için sessionId gönderiyoruz. DB'ye giderken ise cache'den userId bilgisini çekeceğiz
  openRoom(roomCode) {
    this.emit("openRoom", { roomCode });
  }

  closeRoom(roomCode) {
    this.emit("closeRoom", { userSessionId, roomCode });
  }

  requestToJoinSession(roomCode, passcode, displayName) {
    this.emit("requestToJoinSession", {
      roomCode,
      ...(passcode && { passcode }),
      ...(displayName && { displayName }),
    });
  }

  joinSession(sessionId, websocketId, participant) {
    this.emit("joinSession", {
      sessionId,
      websocketId,
      participant: {
        streamId: participant.streamId,
        isCamOpen: participant.isCamOpen,
        isMicOpen: participant.isMicOpen,
        isMicAuthorized: participant.isMicAuthorized,
        isCamAuthorized: participant.isCamAuthorized,
        isSharingScreenAuthorized: participant.isSharingScreenAuthorized,
      },
    });
  }

  leaveSession(participantId, roomSessionId, socketId, isClosingSession) {
    this.emit("leaveSession", {
      participantId,
      roomSessionId,
      socketId,
      isClosingSession,
    });
  }

  muteParticipants(roomSessionId, targetSocketId) {
    this.emit("mute", { roomSessionId, socketId: targetSocketId });
  }

  unmuteParticipants(roomSessionId, targetSocketId) {
    this.emit("unmute", { roomSessionId, socketId: targetSocketId });
  }

  camTurnOffParticipants(roomSessionId, targetSocketId) {
    this.emit("turnOffCam", {
      roomSessionId,
      socketId: targetSocketId,
    });
  }

  camTurnOnParticipants(roomSessionId, targetSocketId) {
    this.emit("turnOnCam", { roomSessionId, socketId: targetSocketId });
  }

  shareScreen(roomSessionId, websocketId, streamId) {
    this.emit("shareScreen", {
      roomSessionId,
      socketId: websocketId,
      streamId,
    });
  }

  stopSharingScreen(roomSessionId, websocketId) {
    this.emit("stopSharingScreen", {
      roomSessionId,
      socketId: websocketId,
    });
  }

  endSession(roomSessionId) {
    this.emit("endSession", {
      sessionId: roomSessionId,
    });
  }

  switchMicAuthorization(roomSessionId, targetSocketId, isMicAllowed, all) {
    this.emit("switchMicAuthorization", {
      roomSessionId,
      socketId: targetSocketId,
      isMicAllowed,
      all,
    });
  }

  switchCamAuthorization(roomSessionId, targetSocketId, isCamAllowed, all) {
    this.emit("switchCamAuthorization", {
      roomSessionId,
      socketId: targetSocketId,
      isCamAllowed,
      all,
    });
  }

  switchSharingScreenAuthorization(
    roomSessionId,
    targetSocketId,
    isSharingScreenAllowed,
    all
  ) {
    this.emit("switchSharingScreenAuthorization", {
      roomSessionId,
      socketId: targetSocketId,
      isSharingScreenAllowed,
      all,
    });
  }

  changeParticipantRole(roomSessionId, participantId, newRole) {
    this.emit("changeParticipantRole", {
      roomSessionId,
      participantId,
      newRole,
    });
  }

  setUserListeners(listenerCallbacks) {
    for (const eventName in listenerCallbacks) {
      this.#socket.on(eventName, listenerCallbacks[eventName]);
    }
  }

  // listenerCallbacks, her bir eventin ismini içeren bir key ve o event'e ait callback'i içeren bir objedir.
  setRoomListeners(listenerCallbacks) {
    for (const eventName in listenerCallbacks) {
      this.#socket.on(eventName, listenerCallbacks[eventName]);
    }
  }
}

export const websocketService = new WebsocketService();
