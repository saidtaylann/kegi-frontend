const roomRoutes = [
  {
    path: "/create",
    name: "createRoom",
    component: async () => import("../views/rooms/Create.vue"),
  },
  {
    path: "/:room-code/chat",
    name: "roomChat",
    component: async () => import("../views/rooms/Chat.vue"),
  },
  {
    path: "/:room-code/works",
    name: "roomWorks",
    component: async () => import("../views/rooms/Works.vue"),
  },
  {
    path: "/:room-code/m",
    name: "roomMembers",
    component: async () => import("../views/rooms/Members.vue"),
  },
];

export default roomRoutes;
