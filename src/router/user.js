const userRoutes = [
  {
    path: "/signin",
    name: "signin",
    component: async () => import("../views/user/Signin.vue"),
  },
  {
    path: "/signup",
    name: "signup",
    component: async () => import("../views/user/Signup.vue"),
  },
  {
    path: "/verify/:token",
    name: "emailVerified",
    component: async () => import("../views/user/EmailVerified.vue"),
  },
];

export default userRoutes;
