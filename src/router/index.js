import { createRouter, createWebHistory } from "vue-router";
import roomRoutes from "./room";
import accountRoutes from "./account";
import sessionRoutes from "./session";
import userRoutes from "./user";
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("../views/Home.vue"),
    },
    {
      path: "/profile",
      name: "profile",
      component: async () => import("../views/account/Profile.vue"),
    },
    ...accountRoutes,
    ...userRoutes,
    ...roomRoutes,
    ...sessionRoutes,
  ],
});

export default router;
