const userRoutes = [
  {
    path: "/account",
    name: "account",
    component: async () => import("../layouts/Account.vue"),
    props: true,
    children: [
      {
        path: "rooms",
        name: "roomList",
        props: true,
        component: async () => import("../views/rooms/List.vue"),
      },
      {
        path: "profile",
        name: "profile",
        props: true,
        component: async () => import("../views/account/Profile.vue"),
      },
    ],
  },
];

export default userRoutes;
