const sessionRoutes = [
  {
    path: "/:roomCode",
    name: "liveSession",
    component: async () => import("../views/session/Live.vue"),
  },
  {
    path: "/:room-code/s",
    name: "sessionList",
    component: async () => import("../views/session/List.vue"),
  },
  {
    path: "/:room-code/s/:sessionId",
    name: "sessionDetail",
    component: async () => import("../views/session/Detail.vue"),
  },
  {
    path: "/:room-code/s/:sessionId/record",
    name: "sessionRecord",
    component: async () => import("../views/session/Record.vue"),
  },
];

export default sessionRoutes;
