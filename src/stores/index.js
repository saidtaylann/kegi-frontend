import { defineStore } from "pinia";

export const useMainStore = defineStore({
  id: "main",
  state: () => ({
    successMessages: [],

    infoMessages: [],

    warningMessages: [],

    errorMessages: [],
  }),
  getters: {
    getSuccessMessages: (state) => {
      return state.successMessages;
    },

    getInfoMessages: (state) => {
      return state.infoMessages;
    },

    getWarningMessages: (state) => {
      return state.warningMessages;
    },

    getErrorMessages: (state) => {
      return state.errorMessages;
    },
  },
  actions: {
    setErrorMessage(error) {
      this.errorMessages.push(error);
    },

    setWarningMessage(warning) {
      this.warningMessages.push(warning);
    },

    setInfoMessages(info) {
      this.infoMessages.push(info);
    },

    setSuccessMessage(success) {
      this.successMessages.push(success);
    },
  },
});
