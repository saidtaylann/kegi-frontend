import { defineStore } from "pinia";
import { websocketService as ws } from "../services/ws";
import axios from "../api/axios";
import router from "../router/index";
import { useUserStore } from "./user";
import { WebRTCService as rtc } from "../services/webrtc";

const serverUrl = "http://localhost:3000";

export const useRoomStore = defineStore({
  id: "room",
  state: () => ({
    activeSession: {
      participant: {},
      messages: [],
      userPreferences: {
        mic: {
          isOpen: true,
          isAuthorized: true,
        },
        cam: {
          isOpen: true,
          isAuthorized: true,
        },
        sharingScreen: {
          isActive: false,
          isAuthorized: true,
        },
        rightBar: {
          isOpen: false,
          type: "",
          width: "15%",
        },
        bottomBar: {
          isOpen: false,
        },
      },
    },
    error: "",
  }),
  getters: {
    getParticipant: (state) => state.activeSession.participant,

    getParticipants: (state) => state.activeSession.participants,

    getActiveSession: (state) => {
      return state.activeSession;
    },

    getActiveSessionPreferences: (state) => state.activeSession.userPreferences,

    getActiveSessionPreference: (state) => {
      return (preference) => state.activeSession.userPreferences[preference];
    },

    getParticipantRole: (state) => {
      return state.activeSession.participant.role;
    },
  },

  actions: {
    deleteParticipant(participant_id) {
      const participantIndex = this.activeSession.participants.findIndex(
        (p) => p.id === participant_id
      );
      if (participantIndex !== -1)
        this.activeSession.participants.splice(participantIndex, 1);
    },

    addParticipant(participant) {
      this.activeSession.participants.push(participant);
    },

    // body: {is_edu, is_temp, title?, passcode?}
    async createRoom(body, open) {
      try {
        const newRoom = await axios.post(`${serverUrl}/rooms`, body);
        if (body.is_temp) {
          ws.createRoomSession(newRoom.data.room_code, {
            isMicAuthorized: true,
            isCamAuthorized: true,
            isSharingScreenAuthorized: true,
          });
        } else if (open === true) {
          ws.openRoom(newRoom.data.room_code);
        }
      } catch (err) {
        throw err;
      }
    },

    async requestToJoinSession(code, passcode, displayName) {
      ws.requestToJoinSession(code, passcode, displayName);
    },

    async joinSession(sessionId, websocketId, participant) {
      ws.joinSession(sessionId, websocketId, participant);
    },

    async getRoomsOfUser() {
      try {
        const rooms = await axios.get(`${serverUrl}/rooms`);
        console.log("rooms.data", rooms.data);
      } catch (error) {
        console.log("error", error);
      }
    },

    async fetchRoom(code) {
      try {
        const room = await axios.get(`${serverUrl}/rooms/${code}`);
        return room.data;
      } catch (error) {
        console.error("error", error);
        throw error;
      }
    },

    async isMemberOfRoom(code) {
      try {
        const isMember = await axios.get(
          `${serverUrl}/rooms/is-member/${code}`
        );
        return isMember.data?.status;
      } catch (error) {
        console.error(error);
        throw error;
      }
    },

    addSessionMessage(message) {
      this.activeSession.messages.push(message);
    },

    mute(roomSessionId, targetSocketId, isIncludeSelf) {
      this.setActiveSessionPreference("mic", "isOpen", !isIncludeSelf);
      ws.muteParticipants(roomSessionId, targetSocketId);
    },

    unmute(roomSessionId, targetSocketId, isIncludeSelf) {
      this.setActiveSessionPreference("mic", "isOpen", isIncludeSelf);
      ws.unmuteParticipants(roomSessionId, targetSocketId);
    },

    openCam(roomSessionId, targetSocketId, isIncludeSelf) {
      this.setActiveSessionPreference("cam", "isOpen", isIncludeSelf);
      ws.camTurnOnParticipants(roomSessionId, targetSocketId);
    },

    closeCam(roomSessionId, targetSocketId, isIncludeSelf) {
      this.setActiveSessionPreference("cam", "isOpen", !isIncludeSelf);
      ws.camTurnOffParticipants(roomSessionId, targetSocketId);
    },

    shareScreen(roomSessionId, usersocketId, streamId) {
      ws.shareScreen(roomSessionId, usersocketId, streamId);
    },

    stopSharingScreen(roomSessionId, usersocketId) {
      ws.stopSharingScreen(roomSessionId, usersocketId);
    },

    setActiveSessionPreference(preference, property, value) {
      if (!(preference in this.activeSession.userPreferences))
        this.activeSession.userPreferences[preference] = {};
      this.activeSession.userPreferences[preference][property] = value;
    },

    leaveSession(participantId, roomSessionId, websocketId, isClosingSession) {
      ws.leaveSession(
        participantId,
        roomSessionId,
        websocketId,
        isClosingSession
      );
    },

    clearActiveSession() {
      console.log("clear active session tetiklendi");
      this.activeSession = {
        participant: {},
        messages: [],
        userPreferences: {
          mic: {
            isOpen: true,
          },
          cam: {
            isOpen: true,
          },
          rightBar: {
            isOpen: false,
            type: "",
            width: "15%",
          },
          bottomBar: {
            isOpen: false,
          },
          sharingScreen: {
            isActive: false,
            isAuthorized: true,
          },
        },
      };
    },

    endSession() {
      ws.endSession(this.activeSession.room_session_id);
    },

    openRoom(roomCode) {
      ws.openRoom(roomCode);
    },

    switchMicAuthorization(sockets, isAllowed, all) {
      console.log("store mic auth");
      ws.switchMicAuthorization(
        this.activeSession.room_session_id,
        sockets,
        isAllowed,
        all
      );
    },

    switchCamAuthorization(sockets, isAllowed, all) {
      ws.switchCamAuthorization(
        this.activeSession.room_session_id,
        sockets,
        isAllowed,
        all
      );
    },

    switchSharingScreenAuthorization(sockets, isAllowed, all) {
      ws.switchSharingScreenAuthorization(
        this.activeSession.room_session_id,
        sockets,
        isAllowed,
        all
      );
    },

    changeParticipantRole(participantId, newRole) {
      console.log("change participant role");
      ws.changeParticipantRole(
        this.activeSession.room_session_id,
        participantId,
        newRole
      );
    },

    setListeners() {
      const userStore = useUserStore();
      ws.setRoomListeners({
        roomSessionCreated: (session) => {
          console.log("room session created", session);
          Object.assign(this.activeSession, session);
          router.push({
            name: "liveSession",
            params: { roomCode: session.code },
          });
        },

        roomOpened: (session) => {
          console.log("room opened", session);
        },

        joinSessionRequestAccepted: (sessionIdAndCode) => {
          console.log("join session request accepted", sessionIdAndCode);
          Object.assign(this.activeSession, sessionIdAndCode);
        },

        joinedSession: (session) => {
          console.log("session", session);
          Object.assign(this.activeSession, session);
          this.activeSession.participant = session.participants.filter(
            (p) => p.websocket_id === userStore.getWebsocketID
          )[0];

          if (this.activeSession.participant.role === 0) {
            const userPreferences = this.activeSession.userPreferences;

            userPreferences.mic.isOpen =
              session.is_mic_authorized &&
              this.activeSession.participant.is_mic_authorized;
            userPreferences.mic.isAuthorized =
              session.is_mic_authorized &&
              this.activeSession.participant.is_mic_authorized;

            userPreferences.cam.isOpen =
              session.is_cam_authorized &&
              this.activeSession.participant.is_cam_authorized;
            userPreferences.cam.isAuthorized =
              session.is_cam_authorized &&
              this.activeSession.participant.is_cam_authorized;
          }
          localStorage.setItem("IN_MEETING", 1);
        },

        someoneJoinedSession: (participant) => {
          console.log("participant", participant);
          this.addParticipant(participant);
        },

        muted: (sockets) => {
          this.activeSession.participants.forEach((p, index) => {
            if (sockets.includes(p.websocket_id)) {
              this.activeSession.participants[index].is_mic_open = false;
            }
          });
          if (sockets.includes(this.activeSession.participant.websocket_id)) {
            this.activeSession.userPreferences.mic.isOpen = false;
          }
        },

        unmuted: (sockets) => {
          this.activeSession.participants.forEach((p, index) => {
            if (sockets.includes(p.websocket_id)) {
              this.activeSession.participants[index].is_mic_open = true;
            }
          });
          if (sockets.includes(this.activeSession.participant.websocket_id)) {
            this.activeSession.userPreferences.mic.isOpen = true;
          }
        },

        turnedOnCam: (sockets) => {
          this.activeSession.participants.forEach((p, index) => {
            if (sockets.includes(p.websocket_id)) {
              this.activeSession.participants[index].is_cam_open = true;
            }
          });
          if (sockets.includes(this.activeSession.participant.websocket_id)) {
            this.activeSession.userPreferences.cam.isOpen = true;
          }
        },

        turnedOffCam: (sockets) => {
          this.activeSession.participants.forEach((p, index) => {
            if (sockets.includes(p.websocket_id)) {
              this.activeSession.participants[index].is_cam_open = false;
            }
          });
          if (sockets.includes(this.activeSession.participant.websocket_id)) {
            this.activeSession.userPreferences.cam.isOpen = false;
          }
        },

        sharedScreen: (socketAndStream) => {
          const index = this.activeSession.participants.findIndex(
            (p) => p.websocket_id === socketAndStream.socketId
          );
          this.activeSession.participants[index]["sharing_screen"] =
            socketAndStream.streamId;
        },

        stoppedSharingScreen: (userStoppedScreenSocket) => {
          console.log("userStoppedScreenSocket", userStoppedScreenSocket);
          this.activeSession.participants.forEach((p, index, arr) => {
            if (p.websocket_id === userStoppedScreenSocket) {
              console.log("if girdi");
              rtc.removeSharingScreen(arr[index].stream_id);
              arr[index].sharing_screen = "";
            }
          });
          if (userStore.getWebsocketID === userStoppedScreenSocket) {
            this.activeSession.userPreferences.sharingScreen.isActive = false;
          }
        },

        leftSession: async () => {
          console.log("session left");
          await router.replace({ name: "home" });
          this.clearActiveSession();
        },

        sessionEnded: async () => {
          await router.replace({ name: "home" });
          this.clearActiveSession();
        },

        someoneLeftSession: (userLeft) => {
          const index = this.activeSession.participants.findIndex(
            (p) => p.websocket_id === userLeft.websocketId
          );
          rtc.removeParticipant(userLeft.streamId);
          this.activeSession.participants.splice(index, 1);
        },

        switchedMicAuthorization: (socketsAndAuth) => {
          if (socketsAndAuth.all) {
            this.activeSession.participants.forEach((p, index, arr) => {
              if (socketsAndAuth.isMicAllowed === false) {
                if (p.role === 0) {
                  this.activeSession.participants[index].is_mic_open = false;
                }
              }
              arr[index].is_mic_authorized = false;
            });
            if (this.activeSession.participant.role === 0) {
              if (socketsAndAuth.isMicAllowed === false) {
                this.activeSession.userPreferences.mic.isOpen = false;
              }
            }
            this.activeSession.userPreferences.mic.isAuthorized =
              socketsAndAuth.isMicAllowed;
            this.activeSession.is_mic_authorized = socketsAndAuth.isMicAllowed;
          } else {
            const participantSocket = this.getParticipant.websocket_id;
            this.activeSession.participants.forEach((p, index, arr) => {
              if (socketsAndAuth.sockets.includes(p.websocket_id)) {
                if (socketsAndAuth.isMicAllowed === false) {
                  arr[index].is_mic_open = false;
                }
                if (socketsAndAuth.sockets.includes(participantSocket)) {
                  if (socketsAndAuth.isMicAllowed === false) {
                    this.activeSession.userPreferences.mic.isOpen = false;
                  }
                  this.activeSession.userPreferences.mic.isAuthorized =
                    socketsAndAuth.isMicAllowed;
                }
                arr[index].is_mic_authorized = socketsAndAuth.isMicAllowed;
              }
            });
          }
        },

        switchedCamAuthorization: (socketsAndAuth) => {
          if (socketsAndAuth.all) {
            this.activeSession.participants.forEach((p, index, arr) => {
              if (socketsAndAuth.isCamAllowed === false) {
                if (p.role === 0) {
                  this.activeSession.participants[index].is_cam_open = false;
                }
              }
              arr[index].is_cam_authorized = false;
            });
            if (this.activeSession.participant.role === 0) {
              if (socketsAndAuth.isCamAllowed === false) {
                this.activeSession.userPreferences.cam.isOpen = false;
              }
            }
            this.activeSession.userPreferences.cam.isAuthorized =
              socketsAndAuth.isCamAllowed;
            this.activeSession.is_cam_authorized = socketsAndAuth.isCamAllowed;
          } else {
            const participantSocket = this.getParticipant.websocket_id;
            this.activeSession.participants.forEach((p, index, arr) => {
              if (socketsAndAuth.sockets.includes(p.websocket_id)) {
                if (socketsAndAuth.isCamAllowed === false) {
                  arr[index].is_cam_open = false;
                }
                if (socketsAndAuth.sockets.includes(participantSocket)) {
                  if (socketsAndAuth.isCamAllowed === false) {
                    this.activeSession.userPreferences.cam.isOpen = false;
                  }
                  this.activeSession.userPreferences.cam.isAuthorized =
                    socketsAndAuth.isCamAllowed;
                }
                arr[index].is_cam_authorized = socketsAndAuth.isCamAllowed;
              }
            });
          }
        },

        switchedSharingScreenAuthorization: (socketsAndAuth) => {
          console.log("socketsAndAuth", socketsAndAuth);
          if (socketsAndAuth.all) {
            this.activeSession.participants.forEach((p, index, arr) => {
              if (socketsAndAuth.isSharingScreenAllowed === false) {
                if (p.role === 0) {
                  p.sharing_screen &&
                    rtc.stopSharing(this, userStore.getWebsocketID);
                  arr[index].sharing_screen = "";
                }
              }
              arr[index].is_sharing_screen_authorized = false;
            });
            if (this.activeSession.participant.role === 0) {
              if (socketsAndAuth.isSharingScreenAllowed === false) {
                this.activeSession.userPreferences.sharingScreen.isActive = false;
              }
            }
            this.activeSession.userPreferences.sharingScreen.isAuthorized =
              socketsAndAuth.isSharingScreenAllowed;
            this.activeSession.is_sharing_screen_authorized =
              socketsAndAuth.isSharingScreenAllowed;
          } else {
            const participantSocket = this.getParticipant.websocket_id;
            this.activeSession.participants.forEach((p, index, arr) => {
              if (socketsAndAuth.sockets.includes(p.websocket_id)) {
                if (socketsAndAuth.isSharingScreenAllowed === false) {
                  console.log("p", p);
                  p.sharing_screen &&
                    rtc.stopSharing(this, userStore.getWebsocketID);
                  arr[index].sharing_screen = "";
                }
                if (socketsAndAuth.sockets.includes(participantSocket)) {
                  if (socketsAndAuth.isSharingScreenAllowed === false) {
                    this.activeSession.userPreferences.sharingScreen.isActive = false;
                  }
                  this.activeSession.userPreferences.sharingScreen.isAuthorized =
                    socketsAndAuth.isSharingScreenAllowed;
                }
                arr[index].is_sharing_screen_authorized =
                  socketsAndAuth.isSharingScreenAllowed;
              }
            });
          }
        },

        changedParticipantRole: (participantIdAndNewRole) => {
          console.log("participantIdAndNewRole", participantIdAndNewRole);
          this.activeSession.participants.forEach((p, index, arr) => {
            if (participantIdAndNewRole.participantId.includes(p.id)) {
              if (this.activeSession.participant.id === p.id) {
                console.log(
                  "this.activeSession.participant.id",
                  this.activeSession.participant.id
                );
                console.log("p.id", p.id);
                console.log("participantIdAndNewRole", participantIdAndNewRole);
                this.activeSession.participant.role =
                  participantIdAndNewRole.newRole;
              }
              arr[index].role = participantIdAndNewRole.newRole;
            }
          });
        },
      });
    },
  },
});
