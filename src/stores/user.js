import { defineStore } from "pinia";
import router from "../router/index";
import { websocketService as ws } from "../services/ws";
import axios from "../api/axios";

const serverUrl = "http://localhost:3000";

export const useUserStore = defineStore({
  id: "user",
  state: () => ({
    user: {
      name: "",
      lastname: "",
      username: "",
      email: "",
      verified: false,
      role: -1,
      type: "",
      avatar: "",
    },
    websocketID: "",
    isLoaded: false,
  }),
  getters: {
    getUser: (state) => {
      return state.user;
    },
    getUserProperty: (state) => {
      return (property) => state.user[property];
    },
    getWebsocketID: (state) => {
      return state.websocketID;
    },

    getIsLoaded: (state) => {
      return state.isLoaded;
    },
  },
  actions: {
    setUser(user) {
      this.user = {
        name: "",
        lastname: "",
        username: "",
        email: "",
        verified: false,
        role: -1,
        type: "",
        avatar: "",
        ...user,
      };
    },
    setUserProperty(property, value) {
      this.user[property] = value;
    },
    removeUser() {
      this.user.name = "";
      this.user.lastname = "";
      this.user.username = "";
      this.user.email = "";
      this.user.verified = false;
      this.user.role = -1;
      this.user.type = "";
      this.user.avatar = "";
    },
    removeUserProperty(property) {
      if (property === "role") {
        this.user.role = -1;
      } else {
        this.user[property] = "";
      }
    },

    startSession() {
      try {
        console.log("session start");
        ws.startUserSession(localStorage.getItem("LID"));
        this.websocketID = ws.getSocketID();
      } catch (err) {}
    },

    async fetchUser() {
      const resp = await axios.get(`${serverUrl}/users`);
      if (resp.data) {
        this.setUser(resp.data);
      }
    },

    /// body: { email: string, password: string }
    async signin(body) {
      console.log("signed in");
      try {
        const resp = await axios.post(
          `${serverUrl}/auth/login`,
          {
            ...body,
            websocket: ws.getSocketID(),
          },
          { headers: { LID: localStorage.getItem("DID") } }
        );
        if (resp.data.user) {
          this.setUser(resp.data.user);
          localStorage.setItem("LID", resp.data.LID);
          localStorage.setItem("DID", resp.data.DID);
          localStorage.setItem("A_T", resp.data.accessToken);
          console.log(
            'localStorage.getItem("LID")',
            localStorage.getItem("LID")
          );

          router.go(-1);
        }
      } catch (err) {
        if (err.statusCode === 500) {
          this.setError("Bir hata oluştu. Lütfen tekrar deneyin.");
        }
        console.log(err);
      }
    },
    /// body: { name: string, lastName: string, email: string, username: string, password: string, role: number, type: string }
    async signup(body) {
      try {
        const res = await axios.post(`${serverUrl}/users`, body);

        if (res.data) {
          localStorage.removeItem("signupInfo");
          localStorage.setItem("LID", res.data.LID);
          localStorage.setItem("DID", res.data.LID);
          localStorage.setItem("A_T", res.data.accessToken);
          this.setUser(res.data.UID);
          localStorage.setItem("LID", true);
          return true;
        }
      } catch (err) {
        console.log(err);
      }
    },

    async signout() {
      try {
        const resp = await axios.post(
          `${serverUrl}/auth/logout`,
          {},
          { headers: { LID: localStorage.getItem("LID") } }
        );
        if (resp.data.success) {
          this.removeUser();
          router.replace({ name: "home" });
          localStorage.removeItem("LID");
          localStorage.removeItem("A_T");
        }
      } catch (err) {
        console.log("err signout", err);
        throw err;
      }
    },

    async signoutAllDevices() {
      try {
        const resp = await axios.post(`${serverUrl}/auth/logout-all-devices`);
        if (resp.data.success) {
          this.removeUser();
          localStorage.removeItem("LID");
          localStorage.removeItem("A_T");
        }
      } catch (err) {
        console.log(err);
      }
    },

    async verifyEmail(token) {
      try {
        const resp = await axios.post(
          `${serverUrl}/auth/verify-email/${token}`
        );
        if (resp.data?.success) {
          this.setUserProperty("verified", true);
        }
      } catch (err) {
        throw err;
      }
    },
    async resendVerificationCode() {
      try {
        const resp = await axios.post(`${serverUrl}/auth/refresh-verification`);
      } catch (err) {
        console.log("err verification", err);
        throw err;
      }
    },

    async updateAvatar(avatar) {
      try {
        const resp = await axios.post(`${serverUrl}/users/avatar`, avatar, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        if (resp.data?.avatar) {
          this.setUserProperty("avatar", resp.data.avatar);
        }
      } catch (err) {
        throw err;
      }
    },

    async updateUser(field) {
      try {
        const resp = await axios.put(`${serverUrl}/users`, field);
        if (resp.data) {
          this.setUser(resp.data);
        }
      } catch (err) {
        console.log(err);
        throw err;
      }
    },

    setListeners() {
      ws.setUserListeners({
        userSessionStarted: async (userId) => {
          try {
            console.log("sessionStarted");
            if (userId > -1) {
              await this.fetchUser();
            }
            this.isLoaded = true;
          } catch (err) {
            this.isLoaded = true;
            console.log("err aldık", err);
          }
        },
      });
    },
  },
});
