resource "aws_key_pair" "frontend" {
  key_name   = "kegi-frontend_ssh_key"
  public_key = file(var.frontend_key_file)
}

resource "aws_security_group" "frontend" {
  name        = "kegi-frontend-sg"
  description = "Allow"
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "frontend1" {
  ami                    = "ami-04e601abe3e1a910f"
  instance_type          = "t2.micro"
  availability_zone      = "eu-central-1a"
  key_name               = aws_key_pair.frontend.key_name
  vpc_security_group_ids = [aws_security_group.frontend.id]
  tags = {
    Name = "Kegi Frontend 1"
  }
}